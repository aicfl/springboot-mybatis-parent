package com.hkb.springboot.service;

import com.hkb.springboot.bean.PageResultBean;
import com.hkb.springboot.dto.SysLogDTO;
import com.hkb.springboot.entity.SysLogEntity;

/**
 * 系统日志接口 . <br>
 * 
 * @author hkb <br>
 */
public interface SysLogService {

    /**
     * 插入SysLog到数据库 <br>
     * 
     * 
     * @param sysLogEntity
     * @return
     */
    Long insertSysLog(SysLogEntity sysLogEntity);

    /**
     * 获得SysLog数据集合
     * 
     * @param sysLogDTO
     * @return
     */
    PageResultBean<SysLogEntity> selectSysLogByPage(SysLogDTO sysLogDTO);

}