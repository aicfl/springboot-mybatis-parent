package com.hkb.springboot.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.hkb.springboot.bean.PageResultBean;
import com.hkb.springboot.dao.SysLogDAO;
import com.hkb.springboot.dto.SysLogDTO;
import com.hkb.springboot.entity.SysLogEntity;
import com.hkb.springboot.service.SysLogService;
import com.hkb.springboot.util.PageUtils;

/**
 * 系统接口实现类 . <br>
 * 
 * @author hkb <br>
 */
@Service
public class SysLogServiceImpl implements SysLogService {

    /**
     * 注入dao
     */
    @Autowired
    private SysLogDAO sysLogDAO;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Long insertSysLog(SysLogEntity sysLogEntity) {
        sysLogDAO.insertSysLog(sysLogEntity);
        return sysLogEntity.getId();
    }

    @Override
    public PageResultBean<SysLogEntity> selectSysLogByPage(SysLogDTO sysLogDTO) {
        PageHelper.startPage(PageUtils.getPageNum(), PageUtils.getPageSize());
        List<SysLogEntity> result = sysLogDAO.selectSysLog(sysLogDTO);
        return new PageResultBean<SysLogEntity>(result);
    }

}