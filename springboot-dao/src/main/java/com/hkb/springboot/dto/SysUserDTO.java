package com.hkb.springboot.dto;

import lombok.Data;

/**
 * 系统用户DTO . <br>
 * 
 * @author hkb <br>
 */
@Data
public class SysUserDTO {

    /**
     * 用户名
     */
    private String username;

}
