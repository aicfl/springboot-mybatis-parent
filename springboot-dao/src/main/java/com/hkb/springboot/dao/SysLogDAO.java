package com.hkb.springboot.dao;

import java.util.List;

import com.hkb.springboot.dto.SysLogDTO;
import com.hkb.springboot.entity.SysLogEntity;

/**
 * 系统日志dao . <br>
 * 
 * @author hkb <br>
 */
public interface SysLogDAO {

    /**
     * 插入SysLog到数据库 <br>
     * 
     * 
     * @param sysLogEntity
     * @return
     */
    void insertSysLog(SysLogEntity sysLogEntity);

    /**
     * 获得SysLog数据集合
     * 
     * @param sysLogDTO
     * @return
     */
    List<SysLogEntity> selectSysLog(SysLogDTO sysLogDTO);
}