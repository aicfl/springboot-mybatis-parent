package com.hkb.springboot.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hkb.springboot.bean.PageResultBean;
import com.hkb.springboot.bean.ResultBean;
import com.hkb.springboot.dto.SysLogDTO;
import com.hkb.springboot.entity.SysLogEntity;
import com.hkb.springboot.service.SysLogService;

/**
 * 系统日志controller . <br>
 * 
 * @author hkb <br>
 */
@RestController
public class SysLogController {

    /**
     * 注入系统日志接口
     */
    @Autowired
    private SysLogService sysLogService;

    @GetMapping("/sys/log")
    public ResultBean<PageResultBean<SysLogEntity>> selectSysLogByPage(SysLogDTO sysLogDTO) {
        return new ResultBean<PageResultBean<SysLogEntity>>(sysLogService.selectSysLogByPage(sysLogDTO));
    }

}
