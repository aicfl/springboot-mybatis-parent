package com.hkb.springboot.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hkb.springboot.util.StringUtils;

/**
 * 首页controller . <br>
 * 
 * @author hkb <br>
 */
@RestController
public class IndexController {

    @GetMapping("/test")
    public Integer getTest() {
        String string = "12a";
        return StringUtils.stringToInteger(string);
    }

}
